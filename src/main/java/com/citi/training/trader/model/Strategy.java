package com.citi.training.trader.model;

import java.util.Date;

public interface Strategy {
	
	public enum StrategyType {
		SIMPLE, TWO_MOVING_AVERAGES
	}

	void addProfitLoss(double profitLoss);

	void stop();

	boolean hasPosition();

	boolean hasShortPosition();

	boolean hasLongPosition();

	void takeShortPosition();

	void takeLongPosition();

	void closePosition();
	
	Stock getStock();
	
	int getId();

}
