package com.citi.training.trader.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.citi.training.trader.dao.SimpleStrategyDao;
import com.citi.training.trader.model.SimpleStrategy;

@Component
public class SimpleStrategyService {

    @Autowired
    private SimpleStrategyDao simpleStrategyDao;

    public List<SimpleStrategy> findAll(){
        return simpleStrategyDao.findAll();
    }

    public int save(SimpleStrategy strategy) {
        return simpleStrategyDao.save(strategy);
    }
}
