package com.citi.training.trader.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.citi.training.trader.dao.TwoMovingAveragesStrategyDao;
import com.citi.training.trader.model.TwoMovingAveragesStrategy;

@Component
public class TwoMovingAveragesStrategyService {
	
	@Autowired 
	private TwoMovingAveragesStrategyDao twoMovingAveragesStrategyDao;
	
	public List<TwoMovingAveragesStrategy> findAll(){
        return twoMovingAveragesStrategyDao.findAll();
    }

    public int save(TwoMovingAveragesStrategy strategy) {
        return twoMovingAveragesStrategyDao.save(strategy);
    }

}
