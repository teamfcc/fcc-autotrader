package com.citi.training.trader.dao;

import java.util.List;

import com.citi.training.trader.model.TwoMovingAveragesStrategy;

public interface TwoMovingAveragesStrategyDao {

	List<TwoMovingAveragesStrategy> findAll();

    int save(TwoMovingAveragesStrategy strategy);
	
}
