package com.citi.training.trader.strategy;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import com.citi.training.trader.exceptions.TradeNotFoundException;
import com.citi.training.trader.messaging.TradeSender;
import com.citi.training.trader.model.Price;
import com.citi.training.trader.model.Stock;
import com.citi.training.trader.model.Trade;
import com.citi.training.trader.model.Trade.TradeState;
import com.citi.training.trader.model.TwoMovingAveragesStrategy;
import com.citi.training.trader.service.PriceService;
import com.citi.training.trader.service.TradeService;
import com.citi.training.trader.service.TwoMovingAveragesStrategyService;

@Profile("two-moving-averages")
@Component
public class TwoMovingAveragesStrategyAlgorithm {

	private static final Logger logger = LoggerFactory.getLogger(TwoMovingAveragesStrategyAlgorithm.class);

	@Autowired
	private TradeSender tradeSender;

	@Autowired
	private PriceService priceService;

	@Autowired
	private TwoMovingAveragesStrategyService strategyService;

	@Autowired
	private TradeService tradeService;

	private ArrayList<String> avgComparisonResults = new ArrayList<>();

	@Scheduled(fixedRateString = "${simple.strategy.refresh.rate_ms:5000}")
	public void run() {

		for (TwoMovingAveragesStrategy strategy : strategyService.findAll()) {

			Trade lastTrade = null;
			try {
				lastTrade = tradeService.findLatestByStrategyId(strategy.getId());
			} catch (TradeNotFoundException ex) {
				logger.debug("No Trades for strategy id: " + strategy.getId());
			}

			if (lastTrade != null) {
				if (lastTrade.getState() == TradeState.WAITING_FOR_REPLY) {
					logger.debug("Waiting for last trade to complete, do nothing");
					continue;
				}

				// account for last trade?
				if (!lastTrade.getAccountedFor() && lastTrade.getState() == TradeState.FILLED) {
					logger.debug("Accounting for Trade: " + lastTrade);
					if (!strategy.hasPosition()) {
						if (lastTrade.getTradeType() == Trade.TradeType.SELL) {
							logger.debug("Confirmed short position for strategy: " + strategy);
							strategy.takeShortPosition();
						} else {
							logger.debug("Confirmed long position for strategy: " + strategy);
							strategy.takeLongPosition();
						}
						strategy.setLastTradePrice(lastTrade.getPrice());
					} else if (strategy.hasLongPosition()) {
						logger.debug("Closing long position for strategy: " + strategy);
						logger.debug(
								"Bought at: " + strategy.getLastTradePrice() + ", sold at: " + lastTrade.getPrice());
						closePosition(lastTrade.getPrice() - strategy.getLastTradePrice(), strategy);
					} else if (strategy.hasShortPosition()) {
						logger.debug("Closing short position for strategy: " + strategy);
						logger.debug(
								"Sold at: " + strategy.getLastTradePrice() + ", bought at: " + lastTrade.getPrice());
						closePosition(strategy.getLastTradePrice() - lastTrade.getPrice(), strategy);
					}
				}

				lastTrade.setAccountedFor(true);
				tradeService.save(lastTrade);
			}

			if (strategy.getStopped() != null) {
				if (!strategy.hasPosition()) {
					continue;
				}
				if (strategy.hasLongPosition()) {
					logger.debug("Closing long position for strategy: " + strategy);
					logger.debug("Bought at: " + strategy.getLastTradePrice() + ", sold at: " + lastTrade.getPrice());
					closePosition(lastTrade.getPrice() - strategy.getLastTradePrice(), strategy);
				} else if (strategy.hasShortPosition()) {
					logger.debug("Closing short position for strategy: " + strategy);
					logger.debug("Sold at: " + strategy.getLastTradePrice() + ", bought at: " + lastTrade.getPrice());
					closePosition(strategy.getLastTradePrice() - lastTrade.getPrice(), strategy);
				}
			}

			logger.debug("Taking strategic action");
			if (!strategy.hasPosition()) {
				// we have no open position

				DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
				LocalDateTime currentTime = LocalDateTime.now();
				String currentTimeString = currentTime.format(formatter);
				String longAvgEndTime = currentTime.minusMinutes(1).format(formatter);
				String shortAvgEndTime = currentTime.minusSeconds(20).format(formatter);

				// Get the long and short averages
				double longAvg = calculateAvg(strategy.getStock(), longAvgEndTime, currentTimeString);
				double shortAvg = calculateAvg(strategy.getStock(), shortAvgEndTime, currentTimeString);

				// Compare avg
				logger.debug("Comparing averages: Long average: " + longAvg + " Short average: " + shortAvg);
				if (longAvg > shortAvg) {
					logger.debug("Short average is lower than long average");
					avgComparisonResults.add("lower");
				} else if (shortAvg > longAvg) {
					logger.debug("Short average is higher than long average");
					avgComparisonResults.add("higher");
				} else {
					logger.debug("Averages are the same");
					avgComparisonResults.add("same");
				}

				// If there are two results in the comparison results array,
				// check to see if there was a cross in averages
				if (avgComparisonResults.size() > 2) {
					int resultsListSize = avgComparisonResults.size();
					// If short < long and if it is, then check if short > long then buy
					if (avgComparisonResults.get(resultsListSize - 2).equals("lower")
							&& avgComparisonResults.get(resultsListSize - 1).equals("higher")) {
						logger.info(
								"Short average crossed above long average, trading to open long position for strategy: "
										+ strategy);
						// Clear ArrayList to free up memory
						avgComparisonResults = new ArrayList<>();
						makeTrade(strategy, Trade.TradeType.BUY);
					}
					// If short > long and if it is, then check if short < long then sell
					else if (avgComparisonResults.get(resultsListSize - 2).equals("higher")
							&& avgComparisonResults.get(resultsListSize - 1).equals("lower")) {
						logger.info(
								"Short average crossed below long average, trading to open short position for strategy: "
										+ strategy);
						// Clear ArrayList to free up memory
						avgComparisonResults = new ArrayList<>();
						makeTrade(strategy, Trade.TradeType.SELL);
					} else {
						// Averages have not crossed so continue
						logger.debug("Averages have not crossed, taking no action");
						continue;
					}
				}

			} else if (strategy.hasLongPosition()) {
				// we have a long position => close the position by selling
				logger.debug("Trading to close long position for strategy: " + strategy);
				makeTrade(strategy, Trade.TradeType.SELL);
			} else if (strategy.hasShortPosition()) {
				// we have a short position => close the position by buying
				logger.debug("Trading to close short position for strategy: " + strategy);
				makeTrade(strategy, Trade.TradeType.BUY);
			}
			strategyService.save(strategy);
		}
	}

	private double calculateAvg(Stock stock, String startTime, String endTime) {
		List<Price> prices = priceService.findLatest(stock, startTime, endTime);

		double total = 0;
		for (Price price : prices) {
			total += price.getPrice();
		}
		double avg = total / (double) prices.size();

		return avg;
	}

	private void closePosition(double profitLoss, TwoMovingAveragesStrategy strategy) {
		logger.debug("Recording profit/loss of: " + profitLoss + " for strategy: " + strategy);
		strategy.addProfitLoss(profitLoss);
		strategy.closePosition();

		if (strategy.getProfit() >= strategy.getExitProfitLoss()) {
			logger.debug("Exit condition reached, exiting strategy");
			strategy.setStopped(new Date());
			strategyService.save(strategy);
		}
	}

	private double makeTrade(TwoMovingAveragesStrategy strategy, Trade.TradeType tradeType) {
		Price currentPrice = priceService.findLatest(strategy.getStock(), 1).get(0);
		tradeSender.sendTrade(new Trade(currentPrice.getPrice(), strategy.getSize(), tradeType, strategy));
		return currentPrice.getPrice();
	}

}
